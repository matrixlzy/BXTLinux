# <center>BXTLinux</center>

> **百晓通Linux**，DIY自 **Tiny Core Linux**（非常感谢**TC**的开发团队能够贡献这么优秀的开源作品）。
> 当前主要作为 **百晓通客栈** 部分课程所需的“实验器材”，预计会在之后添加一些新的有趣的功能，欢迎大家一起来 DIY !!!

![](imgs/VirtualBox_BXTLinux_26_06_2016_00_45_44.png)
![](imgs/VirtualBox_BXTLinux_26_06_2016_00_46_29.png)
![](imgs/VirtualBox_BXTLinux_26_06_2016_00_48_17.png)

### BXTLinux-1.0.ISO 安装镜像下载链接：[http://git.oschina.net/bxtkezhan/BXTLinux/raw/master/release/BXTLinux-1.0.iso](http://git.oschina.net/bxtkezhan/BXTLinux/raw/master/release/BXTLinux-1.0.iso)