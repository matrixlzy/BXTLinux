QEMU:=qemu-system-i386

out.iso: boot/ cde/ extends/
	mkdir build/
	sudo cp -af boot/ build/
	sudo cp -af cde/ build/
	cd extends/ && make
	cd ..
	sudo cp -fv extends/*.tcz build/cde/optional/
	sudo cp -fv extends/*.md5.txt build/cde/optional/
	sudo cp -fv extends/newlist build/cde/copy2fs.lst
	sudo cp -fv extends/newlist build/cde/onboot.lst
	sudo cp -fv extends/newlist build/cde/xbase.lst
	sudo mkisofs -D -r -V "BXTLinux v1.0" -cache-inodes -J -l \
		 -b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat \
		 -no-emul-boot -boot-load-size 4 -boot-info-table -o out.iso build/
	sudo isohybrid out.iso

run: out.iso
	${QEMU} -cdrom out.iso -boot d -m 256

clean:
	sudo rm -rf out.iso build/
	cd extends/ && make clean
